import { Link } from "@inertiajs/inertia-react";

export const Paginator = ({ news }) => {
    const current = news.current_page;
    const prev = news.links[0].url;
    const prev2 = news.links[current - 2 < 0 ? 0 : current - 2].url;
    const next = news.links[news.links.length - 1].url;
    const next2 = news.links[current + 2].url;

    return (
        <div className="btn-group flex justify-center">
            {current + 2 > news.last_page && <Link href={prev2} className="btn btn-outline">{current - 2}</Link>}
            {current != 1 && <Link href={prev != null && prev} className="btn btn-outline">{current - 1 != 0 && current - 1}</Link>}
            <Link className="btn btn-active">{current}</Link>
            {current + 1 < news.last_page && <Link href={next} className="btn btn-outline">{current + 1}</Link>}
            {current + 2 < news.last_page && <Link href={next2} className="btn btn-outline">{current + 2}</Link>}
        </div>
    );
};
