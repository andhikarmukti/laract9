import { ReactComponent } from "@inertiajs/inertia-react";

const isNews = (news) => {
    return news.map((news, i) => {
        return (
            <div key={i} className="card w-full lg:w-96 bg-base-100 shadow-xl">
                <figure>
                    <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
                </figure>
                <div className="card-body">
                    <h2 className="card-title">
                        {news.title}
                        <div className="badge badge-secondary">
                            NEW
                        </div>
                    </h2>
                    <p>{news.description}</p>
                    <div className="card-actions justify-end">
                        <div className="badge badge-outline">{news.category}</div>
                        <div className="badge badge-outline">{news.author}</div>
                    </div>
                </div>
            </div>
        );
    });
};

const noNews = () => {
    return (
        <div>
            Saat ini belum ada berita tersedia.
        </div>
    )
}

const Newslists = (datanews) => {
    return !datanews.news ? noNews() : isNews(datanews.news)
};

export default Newslists;
