import Newlists from "@/Components/Homepage/Newslists";
import { Paginator } from "@/Components/Homepage/Paginator";
import Navbar from "@/Components/Navbar";
import { Head, Link } from "@inertiajs/inertia-react";
import React from "react";

export default function Homepage(props) {
    console.log(props);
    return (
        <div className="bg-slate-100">
            <Head title={props.title} />
            <Navbar />
            <div className="flex justify-center flex-col lg:flex-row lg:flex-wrap lg:items-stretch items-center p-4 gap-4">
                <Newlists news={props.datanews.data} />
            </div>
            <div>
                <Paginator news={props.datanews} />
            </div>
        </div>
    );
}
